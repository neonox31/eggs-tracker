import cv2
import cvutils as cvutils
from skimage.feature import local_binary_pattern
import numpy as np
from matplotlib import pyplot as plt

radius = 3
no_points = 8 * radius

histsA = []
histsB = []
histsC = []
histsD = []

train_images_paths = cvutils.imlist("./train")
for train_image_path in train_images_paths:
    img_gray = cv2.imread(train_image_path, 0)
    img_lbp = local_binary_pattern(img_gray, no_points, radius)

    (h, w) = img_lbp.shape[:2]
    cellSizeX = w / 2
    cellSizeY = h / 2

    # ---------
    # | A | B |
    # ---------
    # | C | D |
    # ---------
    rois = [
        # A
        [(0 * cellSizeX, 0 * cellSizeY), (1 * cellSizeX, 1 * cellSizeY)],
        # B
        [(1 * cellSizeX, 0 * cellSizeY), (2 * cellSizeX, 1 * cellSizeY)],
        # C
        [(0 * cellSizeX, 1 * cellSizeY), (1 * cellSizeX, 2 * cellSizeY)],
        # D
        [(1 * cellSizeX, 1 * cellSizeY), (2 * cellSizeX, 2 * cellSizeY)]
    ]

    for idx, roi in enumerate(rois):
        # create a mask
        mask = np.zeros(img_lbp.shape[:2], np.uint8)
        mask[int(roi[0][1]):int(roi[1][1]), int(roi[0][0]):int(roi[1][0])] = 255
        masked_img = cv2.bitwise_and(img_lbp, img_lbp, mask=mask)

        # calc hist mask
        hist_mask = cv2.calcHist([img_lbp.astype('uint8')], [0], mask, [256], [0, 256])

        if idx == 0:
            histsA.append(hist_mask)
        elif idx == 1:
            histsB.append(hist_mask)
        elif idx == 2:
            histsC.append(hist_mask)
        elif idx == 3:
            histsD.append(hist_mask)

avgA = np.mean(np.array(histsA), axis=0)
avgB = np.mean(np.array(histsB), axis=0)
avgC = np.mean(np.array(histsC), axis=0)
avgD = np.mean(np.array(histsD), axis=0)

# read the image and define the stepSize and window size
# (width,height)
test_img_gray = cv2.imread("test/oeufs-paille.jpg", 0)  # your image path
test_img_lbp = local_binary_pattern(test_img_gray, no_points, radius)
tmp = test_img_lbp  # for drawing a rectangle

# cv2.imshow('LBO', test_img_lbp)
# key = cv2.waitKey(1000)  # pauses for 3 seconds before fetching next image

stepSize = 30
(w_width, w_height) = (80, 80)  # window size
for x in range(0, test_img_lbp.shape[1] - w_width, stepSize):
    for y in range(0, test_img_lbp.shape[0] - w_height, stepSize):
        window = test_img_lbp[x:x + w_width, y:y + w_height, :]

        w_cellSizeX = w_width / 2
        w_cellSizeY = w_height / 2

        # ---------
        # | A | B |
        # ---------
        # | C | D |
        # ---------
        test_img_rois = [
            # A
            [(0 * w_cellSizeX, 0 * w_cellSizeY), (1 * w_cellSizeX, 1 * w_cellSizeY)],
            # B
            [(1 * w_cellSizeX, 0 * w_cellSizeY), (2 * w_cellSizeX, 1 * w_cellSizeY)],
            # C
            [(0 * w_cellSizeX, 1 * w_cellSizeY), (1 * w_cellSizeX, 2 * w_cellSizeY)],
            # D
            [(1 * w_cellSizeX, 1 * w_cellSizeY), (2 * w_cellSizeX, 2 * w_cellSizeY)]
        ]

        for idx, test_roi in enumerate(test_img_rois):
            # create a mask
            mask = np.zeros(window.shape[:2], np.uint8)
            mask[int(test_roi[0][1]):int(test_roi[1][1]), int(test_roi[0][0]):int(test_roi[1][0])] = 255
            masked_window = cv2.bitwise_and(window, window, mask=mask)

            # calc hist mask
            hist_window_mask = cv2.calcHist([window.astype('uint8')], [0], mask, [256], [0, 256])

            if idx == 0:
                histsA.append(hist_mask)
            elif idx == 1:
                histsB.append(hist_mask)
            elif idx == 2:
                histsC.append(hist_mask)
            elif idx == 3:
                histsD.append(hist_mask)

        # cv2.imshow('window', window)
        # key = cv2.waitKey(1000)  # pauses for 3 seconds before fetching next image
        # if key == 27:  # if ESC is pressed, exit loop
        #     cv2.destroyAllWindows()
        #     break

        # classify content of the window with your classifier and
        # determine if the window includes an object (cell) or not
        # draw window on image
        # cv2.rectangle(tmp, (x, y), (x + w_width, y + w_height), (255, 0, 0), 2)  # draw rectangle on image
        # plt.imshow(np.array(tmp).astype('uint8'))
# show all windows
# plt.show()
